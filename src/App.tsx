import React from "react";
import { GlobalStyle } from "./global.styles";
import HomePage from "./pages/home-page/home-page.component";

const App: React.FC = () => {
  return (
    <React.Fragment>
      <GlobalStyle />
      <HomePage />
    </React.Fragment>
  );
};

export default App;
