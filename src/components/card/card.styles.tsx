import styled from "styled-components";

export const CardImage = styled.div`
  border-radius: 8px 8px 0 0;
  position: absolute;
  top: 0;
  left: 0;

  width: 100%;
  height: 100%;
  background-size: cover;
  background-position: center;
  z-index: 2;
  background-image: ${({ imageUrl }: { imageUrl: string }) =>
    `url(${imageUrl})`};
`;
export const ImageContainer = styled.div`
  height: 200px;
  width: ${({ list }: { list: boolean }) => (list ? "32%" : "100%")};
  position: relative;
`;

export const BlurHashContainer = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  z-index: 1;
`;

export const CardContainer = styled.div`
  border-radius: 8px;
  box-shadow: 0 4px 12px 0 rgba(32, 33, 37, 0.06);
  width: ${({ list }) => (list ? "100%" : "calc((99.99% - 60px) / 3)")};
  margin: 10px;
  overflow: hidden;
  cursor: pointer;
  position: relative;
  display: flex;
  flex-direction: ${({ list }: { list: boolean }) => (list ? "row" : "column")};

  &:hover ${CardImage} {
    transform: scale(1.1);
    transition: all 1.5s ease-in-out;
  }

  @media screen and (max-width: 800px) {
    width: 100%;
    flex-direction: column;
  }
`;

export const CardBody = styled.div`
  background: white;
  border: 1px solid #e4e4e4;
  border-top: none;
  width: 100%;
  border-radius: 0 0 8px 8px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const CardTextContainer = styled.div`
  padding: 0.875rem 1rem;
  padding-bottom: 1rem;
`;

export const CardTitle = styled.div`
  font-weight: 500;
  font-size: 1.125rem;
  color: #3c3d40;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
`;

export const CardDescription = styled.div`
  font-weight: 400;
  font-size: 1rem;
  color: #868780;
  margin-top: 2px;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
`;

export const CardFooter = styled.div`
  border-top: 1px dashed #e4e4e4;
  font-size: 12px;
  color: #868789;
  padding: 0.5rem 1rem;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
`;

export const TagsContainer = styled.div`
  font-size: 12px;
  color: #868789;
`;
