import React from "react";
import { Blurhash } from "react-blurhash";
import { Restaurant } from "../../types";
import { BlurHashContainer, CardBody, CardContainer, CardDescription, CardFooter, CardImage, CardTextContainer, CardTitle, ImageContainer, TagsContainer } from "./card.styles";

interface CardProps {
  item: Restaurant;
  list: boolean;
}

const Card: React.FC<CardProps> = ({
  item: { image, name, description, tags, blurhash, ...otherProps }, list = false
}) => {
  return (
    <CardContainer list={list}>
      <ImageContainer list={list}>
        <CardImage imageUrl={image} />
        <BlurHashContainer>
          <Blurhash hash={blurhash} height="200px" width="100%" punch={1} />
        </BlurHashContainer>
      </ImageContainer>
      <CardBody>
        <CardTextContainer>
          <CardTitle>{name}</CardTitle>
          <CardDescription>{description}</CardDescription>
        </CardTextContainer>
        <CardFooter>
          <TagsContainer>{tags.join(", ")}</TagsContainer>
        </CardFooter>
      </CardBody>
    </CardContainer>
  );
};

export default Card;
