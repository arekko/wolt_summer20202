import { render } from "@testing-library/react";
import React from "react";
import HomePage from "./home-page.component";

it("snapshot test", () => {
  const { container } = render(<HomePage />);
  expect(container.firstChild).toMatchSnapshot();
});
