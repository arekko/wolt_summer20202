import React, { useEffect, useState } from "react";
import Card from "../../components/card/card.component";
import restourantsData from "../../restaurants.json";
import { OrderBy, Restaurant } from "../../types";
import { filterAscDesc } from "../../utils/filterAscDesc";
import {
  Button,
  FilterButtonContainer,
  FilterIconAZContainer,
  FilterIconZAContainer,
  GridIconContainer,
  HeaderContainer,
  HomePageContainer,
  ListIconContainer,
  LogoContainer,
  PageInnerContainer,
  Row,
  Text,
  TilteContainer
} from "./home-page.styles";

interface HomePageProps {}

const HomePage: React.FC<HomePageProps> = () => {
  const [data, setData] = useState<Restaurant[] | null>(null);
  const [hasListView, setHasListView] = useState<boolean>(false);
  const [filter, setFilter] = useState<OrderBy>(OrderBy.Asc);

  useEffect(() => {
    setData(restourantsData.restaurants);
  }, []);

  useEffect(() => {
    if (!data) return;
    setData(filterAscDesc(data, filter));
    console.log("data", data);
  }, [data, filter]);

  const toggleFilter = () =>
    setFilter(filter === OrderBy.Asc ? OrderBy.Desc : OrderBy.Asc);

  const toggleListView = () => setHasListView(true);
  const toggleGridView = () => setHasListView(false);

  if (!data) return null;

  return (
    <HomePageContainer>
      <LogoContainer>Wolt Summer 2020</LogoContainer>
      <HeaderContainer>
        <TilteContainer>Dinner near you in Helsinki</TilteContainer>
        <div>
          <Button onClick={toggleGridView} selected={!hasListView}>
            <GridIconContainer />
          </Button>
          <Button onClick={toggleListView} selected={hasListView}>
            <ListIconContainer />
          </Button>
        </div>
      </HeaderContainer>
      <Row>
        <Text>Restaurants open & online</Text>
        <FilterButtonContainer onClick={toggleFilter} selected>
          {filter === OrderBy.Asc ? (
            <FilterIconAZContainer />
          ) : (
            <FilterIconZAContainer />
          )}
        </FilterButtonContainer>
      </Row>
      <PageInnerContainer>
        {data.map((item: Restaurant) => (
          <Card
            key={item.description + item.blurhash}
            item={item}
            list={hasListView}
          />
        ))}
      </PageInnerContainer>
    </HomePageContainer>
  );
};

export default HomePage;
