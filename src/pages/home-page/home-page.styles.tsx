import styled from "styled-components";
import { ReactComponent as FilterIconZA } from "../../assets/sort-alpha-down-alt-solid.svg";
import { ReactComponent as FilterIconAZ } from "../../assets/sort-alpha-down-solid.svg";
import { ReactComponent as ListIcon } from "../../assets/th-list-solid.svg";
import { ReactComponent as GridIcon } from "../../assets/th-solid.svg";

export const HomePageContainer = styled.div`
  max-width: 1200px;
  margin: 0 auto;
  padding: 20px 30px 0;
  min-height: calc(100vh - 260px);

  @media screen and (max-width: 800px) {
    padding: 0;
  }
`;

export const LogoContainer = styled.h1`
  color: black;
  font-weight: 600;
  padding: 20px;
`;

export const FilterIconZAContainer = styled(FilterIconZA)`
  width: 15px;
  height: 15px;
`;
export const FilterIconAZContainer = styled(FilterIconAZ)`
  width: 15px;
  height: 15px;
`;

export const GridIconContainer = styled(GridIcon)`
  width: 15px;
  height: 15px;
`;

export const ListIconContainer = styled(ListIcon)`
  width: 15px;
  height: 15px;
`;

export const Row = styled.div`
  padding: 10px 20px 10px 20px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: "center";
`;

export const TilteContainer = styled.h2`
  font-weight: 400;
  bottom: 16px;
  font-size: 1.75rem;
  line-height: 100%;
  color: #202125;
  margin: 0;

  @media screen and (max-width: 800px) {
    font-size: 1.1rem;
  }
`;

export const Text = styled.h3`
  color: #202125;
  line-height: 1.4;
  margin-top: 10px;
  margin-bottom: 10px;
  font-size: 1.3rem;
`;

export const Button = styled.button`
  color: ${({ selected }: { selected: boolean }) =>
    selected ? "#009de0;" : "#202125"};
  border-color: #e6e6e6;
  fill: #009de0;
  padding: 5px 10px 5px 10px;
  outline: none;
  cursor: pointer;
  margin: 2px;

  &:hover {
    box-shadow: 0 2px 4px 0 #f2f2f2;
  }
  @media screen and (max-width: 800px) {
    display: none;
  }
`;

export const FilterButtonContainer = styled(Button)`
  width: 35px;
  height: 35px;
  padding: 0;
  
  @media screen and (max-width: 800px) {
    display: block;
  }
`;

export const HeaderContainer = styled.div`
  border-bottom: 1px solid rgba(32, 33, 37, 0.12);
  padding: 10px 20px 10px 20px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;
export const PageInnerContainer = styled.div`
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
`;
