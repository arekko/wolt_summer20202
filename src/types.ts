export interface Restaurant {
  blurhash: string;
  city: string;
  currency: string;
  delivery_price: number;
  description: string;
  image: string;
  location: number[];
  name: string;
  online: boolean;
  tags: string[];
}

export enum OrderBy {
  Asc = "asc",
  Desc = "desc"
}

export enum ItemView {
  Grid = "grid",
  List = "list"
}
