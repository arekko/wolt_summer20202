import { OrderBy } from "../types";

export const filterAscDesc = (data: any[] | null, orderBy: OrderBy) => {
  if (!data) return null;
  return data.sort((a, b) =>
    orderBy === OrderBy.Asc
      ? a.name.toLowerCase().localeCompare(b.name.toLowerCase())
      : b.name.toLowerCase().localeCompare(a.name.toLowerCase())
  );
};
