import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
	body {
		padding: 10px 40px;
		font-family: 'Lato', sans-serif;
		@media screen and (max-width: 800px) {
			padding: 0;
		}
	}
	a {
		text-decoration: none;
		color: black;
	}
	* {
		box-sizing: border-box;
	}
`;
