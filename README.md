# Wolt Summer 2020

## Description

Minimalistic app that render a list of restaurants and has the ability to sort restaurants alphabetically.

## Installation

- Clone the repo in your terminal by clicking the _green_ clone or download button at the top right and copyin the url
- In your terminal, type `git clone URL`
  - replace URL with the url you copied
  - hit enter
- This will copy all the files from this repo down to your computer
- In your terminal, cd into the directory you just created
- Type `npm install` to install all dependencies
- Last, but not least, type `npm start` to run the app locally.

- To look at the code, just open up the project in your favorite code editor!

## Languages & tools

- [typeScript](https://www.typescriptlang.org/) javaScript that scales.
- [react-blurhash](https://github.com/woltapp/react-blurhash) image preload placeholder
- [styled-components](https://styled-components.com/) for styling
- [testing-library](https://testing-library.com/docs/react-testing-library/intro) for React component's testing
